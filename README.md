### Philosophy
---
- Runs in Vanilla Doom.
- Keep the spirit of the game.
	- Must be feasible or seen as a "What could've been" in 1994.

### Notes
---
- [G.I. Joes Be Gone](https://gitlab.com/u344/g.i.-joes-be-gone) comes bundled with this, replacing the G.I. Joe hanging bodies sprites and textures with ones more suitable for the somewhat cartoony art style of Doom, from PSX Doom. Credits to JoeyTD/BeardedDoomguy for the original GZDoom-specific version.
	- The sprite replacements are done through DEHACKED, using sprite string renaming.
- The pistol, shotgun, and super shotgun will instantly fire, consistent with the other weapons in the game. The feel of the weapons are dramatically improved. Note that the firing rate remains the same.
- The pistol will use the unused PISGD0 graphic, making it look less rigid but still vanilla-friendly.
- The super shotgun will no longer skip its "closed" animation/sound during its continuous fire animation.
- The super shotgun's muzzle flash longer overlaps with its reloading sprites.
- Things with egregious hitboxes have been addressed.
	- The Revenant is 75 Doom units tall instead of infamously 56 tall.
	- The Archvile is 70 Doom units tall instead of 56 tall.
	- The Mancubus fireball hitboxes are now more accurate to its sprites (Forgot to document the exact difference. Too bad!). This also prevents the projectiles from clipping through walls easily.
- Numerous adjustments to message strings for consistency.
	- "Mega Armor" is now referred to "Combat Armor" for consistency with the manual.
	- "SuperCharge!" message for the SoulSphere is now "SoulSphere!" for consistency with "MegaSphere!".
	- The powerups: Invisibility, Radiation Shielding Suit, Computer Area Map, Light Amplification Visor now have an exclamation point appended to the end of their pickup messages to match the rest of the powerups.
	- Ammo clips are now generically referred to as "bullets", as calling the item (magazines) "clips" were erroneous.
	- "Picked up 4 shotgun shells" is now "Picked up shotgun shells" to take in account of doubled ammo in difficulties such as ITYTD and Nightmare.
	- "A chainsaw! Find some meat!" is now "You got the chainsaw! Groovy!" to be more consistent with the rest of the weapon pickup messages.
		- Couldn't keep "Find some meat!" as it was too long for DEHACKED to parse. So have an Evil Dead reference instead.
	- MAP11 is no longer "'o' of destruction" on the automap, and is now consistently named "circle of death".
- Sprite brightness consistency fixes.
	- Added fullbrights to the Chaingunner, Spider Mastermind, Cyberdemon, Former Human, Pain Elemental, Lost Soul, bullet puffs, homing missile trails, Lite-amp visors.
	- Removed fullbrights from the Spider Mastermind when she is about to fire.
- The cheat for "IDBEHOLDV" is changed to "IDBEHOLDG". 'G' as in "GodSphere" (Invulnerability sphere) for better separation between Invulnerability and Invisibility.
    - The "IDBEHOLD" prompt message is changed to reflect this.
- The Pain Elemental has its raise state undefined officially making it impossible for it to be resurrected.

### Installation
---
- Chocolate Doom / Crispy Doom
	- Run with `-file UndeadDoom.wad -dehlump UndeadDoom.wad`. Or do `-file UndeadDoom.wad -deh UndeadDoom.deh`.
- Other source ports (GZDoom, PrBoom)
	- Only load `UndeadDoom.wad`. 9/10 source ports will automatically load in the DEHACKED that's built in.

### Compatibility
---
- [Doom Minor Spritefixing Project](https://www.doomworld.com/forum/topic/62403-doom-2-minor-sprite-fixing-project-v20-beta-1-release-updated-11822/)
	- Load Undead Doom AFTER Doom Minor Spritefixing Project. The wrong order will result in the G.I. Joe bodies being thematically unchanged. Undead Doom may also break if you load its DEHACKED instead, which you shouldn't be doing anyway as this contains fullbright fixes, and more, already built in.
- [Per Kristian's High-Res SFX](https://www.perkristian.net/game_doom-sfx.shtml)
	- Load Undead Doom AFTER Perk Kristian's High-Res SFX. The wrong order will result in the super shotgun's closing sound being silent because of the workaround I had to use.
